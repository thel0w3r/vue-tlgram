import firebase from 'firebase'
import 'firebase/firestore'

const config = {
  apiKey: 'AIzaSyAPsrz-nLb6idE4VsjN8b5E5PJahGZNFAo',
  authDomain: 'vue-tlgram.firebaseapp.com',
  databaseURL: 'https://vue-tlgram.firebaseio.com',
  projectId: 'vue-tlgram',
  storageBucket: 'vue-tlgram.appspot.com',
  messagingSenderId: '569709523991'
}

firebase.initializeApp(config)

// firebase utils
const db = firebase.firestore()
const auth = firebase.auth()
const storage = firebase.storage()
const currentUser = auth.currentUser

// date issue fix according to firebase
const settings = {
  timestampsInSnapshots: true
}
db.settings(settings)

// firebase collections
const usersCollection = db.collection('users')
const picturesCollection = db.collection('pictures')
const commentsCollection = db.collection('comments')
const likesCollection = db.collection('likes')

export {
  db,
  auth,
  storage,
  currentUser,
  usersCollection,
  picturesCollection,
  commentsCollection,
  likesCollection,
  firebase
}
