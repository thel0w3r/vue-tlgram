import Vue from 'vue'
import Vuex from 'vuex'
import {auth, usersCollection} from './firebaseConfig.js'

Vue.use(Vuex)

auth.onAuthStateChanged(user => {
  if (user) {
    store.commit('setLoading', true)
    store.commit('setCurrentUser', user)
    store.dispatch('fetchUserProfile')
  } else {
    store.commit('setLoading', false)
  }
})

export const store = new Vuex.Store({
  state: {
    ui: {
      darkVariant: false,
      loading: false
    },
    session: {
      currentUser: null,
      userProfile: {}
    }
  },
  mutations: {
    updateAppTheme (state, theme) {
      state.ui.darkVariant = theme
    },
    setCurrentUser (state, val) {
      state.session.currentUser = val
    },
    setUserProfile (state, val) {
      state.session.userProfile = val
    },
    setLoading (state, val) {
      state.ui.loading = val
    }
  },
  actions: {
    setAppTheme (context, theme) {
      if (theme === 'initial') {
        let localTheme = localStorage.getItem('ui-theme')
        context.commit('updateAppTheme', localTheme === 'dark')
      } else {
        localStorage.setItem('ui-theme', (theme === 'dark') ? 'dark' : 'light')
        context.commit('updateAppTheme', theme === 'dark')
      }
    },
    fetchUserProfile ({ commit, state }) {
      usersCollection.doc(state.session.currentUser.uid).get().then(res => {
        commit('setUserProfile', res.data())
        commit('setLoading', false)
      }).catch(err => {
        commit('setLoading', false)
        console.log(err)
      })
    },
    clearData ({ commit }) {
      commit('setCurrentUser', null)
      commit('setUserProfile', {})
      commit('setLoading', false)
    }
  }
})
