export function checkImageMIME (image) {
  /* let arr = (new Uint8Array(image)).subarray(0, 4)
  let header = ''
  for (let i = 0; i < arr.length; i++) {
    header += arr[i].toString(16)
  }
  let matching = 'NOT_IMAGE'
  switch (header) {
    case '89504e47':
      // PNG
      matching = 'image/png'
      break
    case 'ffd8ffe0':
    case 'ffd8ffe1':
    case 'ffd8ffe2':
      // JPG OR ANY OTHER COMPATIBLE FILES
      matching = 'image/jpeg'
      break
  }
  return matching */
  let imageType = image.substring('data:image/'.length, image.indexOf(';base64'))
  console.log(imageType)
  switch (imageType) {
    case 'jpg':
    case 'jpeg':
    case 'png':
      return 'IMAGE'
    default:
      return 'NOT_IMAGE'
  }
}

export function dataURItoBlob (dataURI) {
  // convert base64 to raw binary data held in a string
  // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
  var byteString = atob(dataURI.split(',')[1])

  // separate out the mime component
  var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]

  // write the bytes of the string to an ArrayBuffer
  var ab = new ArrayBuffer(byteString.length)

  // create a view into the buffer
  var ia = new Uint8Array(ab)

  // set the bytes of the buffer to the correct values
  for (var i = 0; i < byteString.length; i++) {
    ia[i] = byteString.charCodeAt(i)
  }

  // write the ArrayBuffer to a blob, and you're done
  var blob = new Blob([ab], { type: mimeString })
  return blob
}

export function getGUID () {
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4()
}

function s4 () {
  return Math.floor((1 + Math.random()) * 0x10000)
    .toString(16)
    .substring(1)
}
